package com.xingyun.kingdee.erp.test;

import com.alibaba.fastjson.JSONObject;
import com.thebeastshop.forest.springboot.annotation.ForestScan;
import com.xingyun.kingdee.erp.api.model.ErpSupplierData;
import com.xingyun.kingdee.erp.common.model.param.base.ErpLoginParam;
import com.xingyun.kingdee.erp.common.model.param.opt.ErpBillQueryParam;
import com.xingyun.kingdee.erp.common.model.param.opt.ErpSaveFormParam;
import com.xingyun.kingdee.erp.common.model.param.opt.ErpViewParam;
import com.xingyun.kingdee.erp.common.model.param.opt.content.ViewFormParamContent;
import com.xingyun.kingdee.erp.common.model.response.Result;
import com.xingyun.kingdee.erp.test.client.ErpServiceClient;
import com.xingyun.kingdee.erp.test.model.ErpPurchaseOrder;
import com.xingyun.kingdee.erp.tools.param.model.XyQueryWrapper;
import com.xingyun.kingdee.erp.tools.response.model.save.ErpSaveFormResponse;
import com.xingyun.kingdee.erp.tools.response.util.ErpReponseHelper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author lingo
 * @version v1.0
 * @date 2020/8/13 16:02
 * @Company www.xyb2b.com
 */
@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
@ForestScan(basePackages = "com.xingyun.kingdee.erp")
public class TestSpring {

    @Autowired
    private ErpServiceClient erpServiceClient;

    private static ErpLoginParam loginParam = new ErpLoginParam();
    static {
        loginParam.setAcctId("5eb900379fe248");
        loginParam.setPassword("password");
        loginParam.setUsername("account");
    }

    @Test
    public void $1TestView(){
        ErpViewParam erpViewParam = new ErpViewParam();
        erpViewParam.setErpLoginParam(loginParam);
        erpViewParam.setFormId("PUR_PurchaseOrder");
        ViewFormParamContent paramContent = new ViewFormParamContent();
        paramContent.setNumber("PO2008110002");
        erpViewParam.setParamContent(paramContent);
        Result<JSONObject> view = erpServiceClient.view(erpViewParam);
        System.out.println(view.getData());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void $2TestBillQuery(){
        XyQueryWrapper<ErpPurchaseOrder> eq = XyQueryWrapper.of(ErpPurchaseOrder.class).eq(ErpPurchaseOrder::getFid, 104487)
                .select(ErpPurchaseOrder::getFpoId,ErpPurchaseOrder::getFjobNumber)
                .or(i -> i.eq(ErpPurchaseOrder::getFcontrol,true))
                ;
        ErpBillQueryParam param = ErpBillQueryParam.from(loginParam, "PUR_PurchaseOrder", eq.getQueryBillFieldKeys(), eq.buildWhereSql());
        Result<Object> objectResult = erpServiceClient.billQuery(param);
        System.out.println(objectResult.getData());
    }

    @Test
    public void $3TestSave(){
        ErpSupplierData erpSupplierData = new ErpSupplierData();
        erpSupplierData.setName("testKingdee"+ System.currentTimeMillis());
        ErpSaveFormParam<ErpSupplierData> param = ErpSaveFormParam.from("BD_Supplier", erpSupplierData, loginParam, false);
        Result<JSONObject> save = erpServiceClient.save(param);
        ErpSaveFormResponse saveResponseFromResult = ErpReponseHelper.getSaveResponseFromResult(save);
        System.out.println(JSONObject.toJSONString(saveResponseFromResult));
    }

}
