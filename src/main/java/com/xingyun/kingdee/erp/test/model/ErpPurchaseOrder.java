package com.xingyun.kingdee.erp.test.model;

import com.xingyun.kingdee.erp.common.model.annotation.ErpQueryBillField;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author lingo
 * @version v1.0
 * @date 2020/8/13 11:04
 * @Company www.xyb2b.com
 */
@Setter
@Getter
@ToString
public class ErpPurchaseOrder {

    //Fid,FMaterialId.FNumber,FBuyer.FSTAFFNUMBER,FDeliveryControl
    //FBillNo = 'PO2008110002' and FDeliveryControl = '0'


    private String fid;

    @ErpQueryBillField(fieldKey = "FMaterialId.FNumber")
    private String fskuNumber;

    @ErpQueryBillField(fieldKey = "FBillNo")
    private String fpoId;

    @ErpQueryBillField(fieldKey = "FBuyer.FSTAFFNUMBER")
    private String fjobNumber;

    @ErpQueryBillField(fieldKey = "FDeliveryControl")
    private Boolean fcontrol;

}
