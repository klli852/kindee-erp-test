package com.xingyun.kingdee.erp.test;

import com.thebeastshop.forest.springboot.annotation.ForestScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author lingo
 * @version v1.0
 * @date 2020/8/13 15:53
 * @Company www.xyb2b.com
 */
@SpringBootApplication
@ForestScan(basePackages = "com.xingyun.kingdee.erp")
public class ErpTestApplication {
    public static void main(String[] args) {
        SpringApplication.run(ErpTestApplication.class);
    }
}
