package com.xingyun.kingdee.erp.test.client;

import com.alibaba.fastjson.JSONObject;
import com.dtflys.forest.annotation.BaseRequest;
import com.dtflys.forest.annotation.DataObject;
import com.dtflys.forest.annotation.Request;
import com.xingyun.kingdee.erp.common.model.interfaces.ErpApi;
import com.xingyun.kingdee.erp.common.model.param.base.ErpLoginParam;
import com.xingyun.kingdee.erp.common.model.param.opt.*;
import com.xingyun.kingdee.erp.common.model.response.Result;
import org.springframework.stereotype.Component;

import javax.validation.Valid;
import java.util.List;

/**
 * @author lingo
 * @version v1.0
 * @date 2020/8/13 15:57
 * @Company www.xyb2b.com
 */
@Component
@BaseRequest(baseURL = "http://localhost:8181/erp")
public interface ErpServiceClient extends ErpApi {

    @Request(
            url = "/view",
            type = "POST",
            contentType = "application/json",
            dataType = "json"
    )
    @Override
    Result<JSONObject> view(@DataObject ErpViewParam erpViewParam);

    @Request(
            url = "/billQuery",
            type = "POST",
            contentType = "application/json",
            dataType = "json"
    )
    @Override
    Result<Object> billQuery(@DataObject ErpBillQueryParam erpViewParam);


    @Override
    @Request(
            url = "/save",
            type = "POST",
            contentType = "application/json",
            dataType = "json"
    )
    Result<JSONObject> save(@DataObject @Valid ErpSaveFormParam erpSaveFormParam);

    @Override
    Result<JSONObject> audit(@Valid ErpAuditParam erpAuditParam);

    @Override
    Result<JSONObject> unAudit(@Valid ErpUnAuditParam erpUnAuditParam);

    @Override
    Result<JSONObject> submit(@Valid ErpSubmitParam erpSubmitParam);

    @Override
    Result<JSONObject> delete(@Valid ErpDeleteParam erpDeleteParam);

    @Override
    Result<JSONObject> push(@Valid ErpPushDownParam erpPushDownParam);

    @Override
    Result<List<String>> loginToGetCookie(@Valid ErpLoginParam erpLoginParam);
}
